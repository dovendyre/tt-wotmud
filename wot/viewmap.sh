#!/usr/bin/env bash

MAP_FILE='panes/map.txt'
MAP_SIZE='map_size.tin'
REFRESH_RATE=.50

while [ "true" ]; do
    echo \#var MAP_ROWS $(tput cols)\; > $MAP_SIZE
    echo \#var MAP_LINES $(tput lines)\; >> $MAP_SIZE
    clear
    cat $MAP_FILE
    sleep $REFRESH_RATE
done