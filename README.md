# TinTin++ and WotMUD

Tintin++ scripts and map for wotmud.
http://wotmud.org/

A beguinner guide for the game can be found [here..](http://wotmod.org/viewtopic.php?f=76&t=600)

This is a work in progess and currently only alfa, and so is my english ;) I'm totally new to this game, did not find mutch information on how to use tintin++ with this game.

Big thanks to rascul on github, lots of scripts are reused here. [https://github.com/rascul/tintin-scripts](https://github.com/rascul/tintin-scripts)

The map is converted from a zmud map. The map project can you find [here.](http://www.wotmudarchives.org/forum/viewtopic.php?f=23&t=2470)

If you want to help, or have any questions please let me know!

# Prerequisite
* The tintin++ MUD client. On Arch linux based distroes you can install it by typing `sudo pacman -S tintin` in the terminal. If you are using Ubuntu or any other distribution you can take a look [here.](https://tintin.sourceforge.io/install.php#Ubuntu)
* For the `mapsync` command to work, you need to have tail,head and sed installed. Usally allredy installed on your linux system. This is also a prerequisite for the map to find you flee location.
* If you want my setup, whith multiple terminal windows you need tmux. On Arch linux based distoes you can install it by typing `sudo pacman -S tmux` in your terminal. If you are using Ubuntu you can install it by typing `sudo apt install tmux` in the terminal.
* You allso need a character in the game. If you dont have that yet, follow this steps.
 * Type `tintin` in your terminal to start the tintin++ gamle client
 * Type `#session wot game.wotmud.org 2224` inside the client to connect to the game.
 * When connected follow the on-screen instructions. When you are inside the "Circle of Light" you can disconnect the gamme by typing `#end` inside the game client.
 * Update the autocon.tin file with your character name and password. Now you can lauch the game with `./tmuxstart.sh` or `./wot.sh` and your are right back where you left off. From now on, please go to an inn and `rent` to save your gear and progress, if you cant find an inn, type `end` into the client and it saves the map and your progress before disconnecting.

# Install

*   Clone with git

`git clone https://gitlab.com/dovendyre/tt-wotmud.git`

* Goto dir and run the install script

`cd tt-wotmud`

`chmod +x setup.sh`
`chmod +x startwot`

`./setup.sh`

`./startwot`

* When you get out of the circle of light type msync to get the map going
`msync`


# Change char

* This script are intended to make it simpler to update your local scripts after first install, you will be prompted to overwrite existing files

`cd tt-wotmud`

`./setup.sh`


# Help

## The help sections is under construction. Thanks for your patients.

## Tips
* DON'T kill wolfs, they can think and feel.
* Male hummen channelers are wanted. Be carfull if you go that route.

## The UI
* I'm using tmux and have the map in a separate window, you need to edit this (#map map 110x30) numbers in the tmux.tin file to get it working properly. The left number is width and the right number is the hight.
* Run the ./viewmap.sh script in the terminalwindow you would like to view the map
* Run the ./showcomm.sh script in the terminalwindo you would like to view the communication messages. They will allso be displayed in the main window.

## Moving around and mapsyncing
* To move around you can use single char direction commands followed by enter (n,s,e,w,u,d) or hold down shift and use the arrowkeys to move flat (n,s,e,w) and hold the crtl key and use arrows up and down to move vertically (u,d).
* The map should follow you wherever you go, but there might be times it dont.
* You can type "minfo" to print data from the room the map thinks you are inn. The name should match what you see when you type "look".
* If the map and your location is not the same you can type "mapsync" it will take you to the closest room on the map with the same name that you get when you type "look". Beacuse of the triggers used by this function you might need to move in a direction so that the map moves to a new room (That triggers the roomname to be stored in a variable), and then try "mapsync" again.
* When you flee, the map should allso follow you to you new location, if not multiple rooms nearby have the same name, then you need to use the "mapsync" command again.
* Tried to implement a follow function, so that the map follows when you follow somone. It's higly experimental, and does not work when you get teleported somwhere.

## Aliases
### This is work in progress, and most of them can be better but her's a list of them all
* At the bottom of the scripfile.tin there is an alias called "login". There you can change some variables that is most commonly used. example: The variable bag is set to backpack, horse is set to warhorse, light is set to lantern and so on. Change the line to reflect your horse / bagtype / light type. This allso changes your mood to brave (sets your automatic flee HP), activates auto bash retry if target is stunned, activates automatic looting of corpses, sets target 1 to tree and target 2 to bandit. You can change it to whatever you feel like.

#### Bag
* bag bagtype = Sets the bag variable to desired bag type. "bag backpack" Sets the bag variable to backpack.
* ep = Examin the content of your bag.
* pap = Put everything you hold into your bag. (Everything you see when you type "inventory" is put into your bag.)

#### Water
* dw = Take your waterflask from your belt, drink and put it back.
* fwater watersource = Take out your waterflask, fill it from watersource, put it back in your belt. "fwater well" will fill your flask from a well.
* es watersource = Examine the watersource. "es well" Checks if the water in the well is drinkable.

#### Blood
* db = Same as water
* fb = Same as fwater
* eb = same as es

#### Light
* gli lightsource = Sets variable light to a specified lingtsource. "light lantern" Sets the light source to a lantern.
* gli = Gets light from bag (needs variable bag to be specified). and starts to use the light.
* rli = Stop using light and put it into the bag.
* dli = Drops the light you are using on the ground.
* el = Examines light to see how long it lasts.

#### Horse
* horse horsetype = Sets the horsetype you want to interact with. "hose warhorse" Sets the variable horse to warhorse.
* hh = Sets the horse variable to "horse"
* dh = Dismount horsee
* rh = Ride horse, where horse is the horsevariable you defined.
* lh = Lead horse, where horse is the horsevariable you defined.
* eqh number = Gets the horsequipment from the bag (the variable bag needs to be defined), and equip them to the horse. "eqh 3" Gets 3 horseitems from your bag and equips them all.

#### Tickets
* gtp = Gets ticket from bag (the variable bag needs to be defined), and gives it to the stablemaster or a coach driver. If you got multiple tickets you can get the second ticet from the bag with "gtp 2".

#### Social
* buddy NameOfBuddy = Sets the buddy variable to the name of the person you want to interact with.
* tb whaterver = Tells buddy "whatever", quick and easy way to whisper to one person.
* fb = Follows buddy
* ab = Assist buddy (You attack the target your buddy is attacking)
* gb = Make a group with buddy
* rb = Rescue buddy
* wb = Whois buddy (Gets whois information on buddy)
* db = Diagnose buddy

#### Mood
* When you go into battel your mood determins how fast you attempt to flee from a battel - use with care!
* cmw = Change mood to whimpy - Attempts to flee allmost at the same time you are attacked.
* cmn = Change mood to normal - Attempts to flee somewhere in the middle of whimpy and brave..
* cmb = Change mood to brave - Normal battle mode, you attempt to flee when your HP gets quite low.
* cmbb = Change mood to berserk - Does not flee at all! Use with care..
* whimpy number = Sets the target HP for when you want to flee. "whimpy 200" Makes you attempt to flee when HP drops to 200.

#### Doors
* There are actions that will set doortype automaticly when you try to open or discovers a door.
* op = Open door
* kdd = knock door
* pd = Pick door
* cd = Colse door
* ld = Lock door
* ud = Unlock door
* door doorname = Sets the variable door to a spesific door type. Rearly needed

#### Killing
* t1 - t4 target = Sets the variable target1,target2,target3, and target4. "t1 leatherleaf" sets target1 to leatherleaf
* k1 - k4 = Kills target1, target2, target3, og target4. "k1" Kills a leatherleaf and sets current target to leatherleaf.
* kk - Kill current target.
* bt - Bash current target.
* target = sets current target.
* F1 - F4 = By pressing the F1,F2,F3 or F4 key on your keyboard you kill target 1,2,3, or 4.
* F5 = The F5 key kills current target
* F6 = The F6 Key bashes the current target.
* NOTE! The F-keys might not work on all keyboard!

#### Looting
* aloot = Autoloot on. Grabs everything from the mobs you kill.
* noloot = Disables autoloot. You need to manually get what you want from the corpse.

#### Bashing
* bon = If target is stunned you will automaticly retry the bash.
* boff = Turns off automaticly bash retry.
* bashes = Shows you your bashing statistics.
* bashland = Shows you total number of bashes landed.
* bashmiss = Shows you total number of bashes missed.
* resetbashes = Reset the statistics. Needs to be run first, for the couting to start. (scriptfile.tin does this automaticly.)

#### Title
* title = View or change your title

#### Idle
* noidle = Prohibits that your character gets pulled into the void (afk mode), and that your session disconnects. Use with care, if you dont disable it, your attacks and bashes might get interrupted.
* stopnoidle = Disables the noidle.

#### Login
* login = Gives a lot of variables value when you lauch the game with wot.sh. By default it sets water to water (works on most flasks and other water couintainers), food to meat, bag to backpack, light to lantern,horse to warhorse, t1 to tree, t2 to bandit, changes mood to brave and enables autoloot.

#### Map symbols
* INN = A place you can current
* W = Watersource
* K = Kitchen - type `make dinner` to get a free dinner.
* B = Bank
* C = Coach
* St = Stables
* S = Supply shop
* T = Tailor
* H = Herbalist / Hedgedoctor
* Wp = Weapon shop
* MB = Master Blacksmith
* A = Armory
* F = Forge
* Sm = Smithy
* D = A place you can get a drink
* F = A place to buy food
* P = Weave - Portal - Teleportation place
* Pet = Pet shop
* EXP = Trees to kill for easy exp
* WT = Warrior skill Trainer
* TT = Thief trainer
* RT = Ranger/Hunter skill trainer

#### Mapping
* Here are a lot of shortcuts to make it easy to move around on the map and lessen the thyping when mapping. As i'm playing throu the game I will update the map with flags and automatic run paths.
* end = Will disconnect you session, save your character, and save your map. Use ONLY of you cant get to an inn and rest.
* wm = Writes the changes and current possition to the map file. Allso saves your character.
* mm direction = Will move you in a direction on the map, you character will not move. "mm e" will move you east.
* smap = Will dislpay a bigger map in the main window.
* mlist = Lists all rooms in the mapfile. You can narrow it down by typing a name. "mlist Stables" will show you all rooms with exactly Stables as the roomname.
* mgo roomname / roomnumber = Jumps to the the location on the map with roomname or roomnumber. This will only move the map.
* run roomname / roomnumber = Makes your character run to the location you want. NOTE! Long distances will exaust you and your mount. The map is work in progress and not all places you can't go are marked as "avoid", so you might end up totally wrong. You can use "mapsync" to find your location and try again.
* avoid = Makes the run command avoid the room you are currently in.
* favoid direction = Tell the run command that the exit should be avoided. "favoid e" will tell the run command to avoid the east exit.
* fgate direction = Tells the map that you need to open the gate in a direction to get through. Makes you "call" before going that direction.
* fdel direction = Deletes the flag on exit.

#### Running
* Use with caution, you might end up exhausted somewhere or in the middle of an enemy army! Only go to the closest city, dont go from one end of the world to ther other, you will get exhausted. Use the worldmap to get familiar with the world.
* map = will display the worldmap.
* help map = will display exlenations of where the different cities are.
* ramador = Will take you ta amador (SouthWest)
* remfield = Will take you to Emond's Field (West)
* rwatchhill = Will take you to Watch Hill (Between Emond's Field and Baerlon)
* rbaerlon = Will take you to Baerlon (NorthWest)
* rwhitebridge = Will take you to Wihte Brige (Middle of the map)
* rj4 = Will take you to the junction between White Brige and Caemlyn. (Middel of the map)
* rcaemlyn = Will take you to Caemlyn. (East)
* rglancor = Will take you to Glancor. (SouthEast) Between Caemlyn and Tear.
* rtear = Will take you to Tear. (SouthEast) Far down.
* rjehannah = Will take you to Jehannah (SouthEast from Emond's Field)
