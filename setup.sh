#!/bin/bash

echo " "
echo    "############################### "
echo    "          Initial setup         "
echo    " Run this for new char as well  "
echo    "############################### "
echo " "
echo    "############################### "
echo    "  Your password will be stored  "
echo    "         in plain text!         "
echo    "############################### "
echo " "
read -p 'Character name: ' username
read -sp 'Character password: ' password

### Gets the mud folder
ttwotmud=$(dirname $(readlink -f $0))

#Getting what files to use
source $ttwotmud/includes

## Getting char
[ ! -f $ttwotmud/char ] && touch $ttwotmud/char && echo "#!/bin/bash

## Enter your characeters name
export  CHARNAME=\"$username\"" > $ttwotmud/char
source $ttwotmud/char


if [[ ! $CHARNAME = "$username" ]]
then
  sed -i "/CHARNAME/c\export  CHARNAME=\"$username\"" $ttwotmud/char
  export  CHARNAME="$username"
fi

#Go to the mud dir
cd $MUDDIR

## Checks and generate files of dont exist
## Folderes
[ ! -d $CHARDIR$CHARNAME ] && mkdir -p $CHARDIR$CHARNAME
[ ! -d $LOGDIR ] && mkdir -p $LOGDIR
[ ! -d $PANEDIR ] && mkdir -p $PANEDIR
## Roomlog
[ ! -f $LOGDIR$ROOMLOG ] && touch $LOGDIR$ROOMLOG
[ ! -f $LOGDIR$FLEELOG ] && touch $LOGDIR$FLEELOG
## Chatlog
[ ! -f $PANEDIR$CHATFILE ] && touch $PANEDIR$CHATFILE
## Mapfile
[ ! -f $PANEDIR$MAPFILE ] && touch $PANEDIR$MAPFILE
## Character files
[ ! -f $CHARDIR$CHARNAME/$AUTOCON ] && echo "#nop Auto connect to server
#session wot game.wotmud.org 2224;$CHARNAME;$password" > $CHARDIR$CHARNAME/$AUTOCON
[ ! -f $CHARDIR$CHARNAME/$LOGIN ] && echo "#nop *** RUNS AT START - SET THE VARIABLES TO MATCH WHAT YOUR CHARACTER IS USING - ENABLES AUTOLOOT ***
#alias {login} {
dw skin;
dws skin;
ef meat;
bag backpack;
bag2 pouch;
bknbag;
gli lantern;
horse horse;
bkn dagger;
mhw staff;
myname $CHARNAME;
ang statuette;
aloot;
bon;
cmb;
target tree;
t1 tree;
t2 stag;
t3 elk;
t4 bandit;
armor;
blind;
};" > $CHARDIR$CHARNAME/$LOGIN

## Changes the include paths for char specific tin file
sed -i "/autocon.tin/c\#read chars/$CHARNAME/autocon.tin" $MAINTT
sed -i "/login.tin/c\#read chars/$CHARNAME/login.tin" $MAINTT

echo " "
echo " "
echo    "############################### "
echo    "         Run startwot           "
echo    "       To start the game        "
echo    "############################### "